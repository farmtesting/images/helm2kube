FROM bitnami/kubectl:1.16-debian-10

# Labels about the Dockerfile 
LABEL name="Helm and Kubectl." \
    description="Image for using Helm and Kubectl." \
    version="${HELM_VERSION}" \
    maintainer="Thibaut ALLAIN <thibaut.allain@orange.com>" \
    url="https://gitlab.com/farmtesting/images/helm2kube"

ARG HELM_VERSION=v3.2.4

USER root

# Install required system packages and dependencies
RUN install_packages curl jq 

RUN curl -L https://get.helm.sh/helm-${HELM_VERSION}-linux-amd64.tar.gz | tar xz && \ 
    mv linux-amd64/helm /bin/helm && \ 
    rm -rf linux-amd64 && \
    curl -s "https://raw.githubusercontent.com/kubernetes-sigs/kustomize/master/hack/install_kustomize.sh"  | bash

RUN chmod 755 /bin/helm  

ENTRYPOINT []
CMD [ "kubectl --help" ]
